#include "asm/includes.h"
#include "msg.h"
#include "asm/power_api.h"

//本文件仅用于测试部分模块
#if   1
///测试模块编号，部分模块不在这里测试
enum {
    TEST_MCPWM = 1,
    TEST_PWM_LED,
    TEST_TIMER_CAP,
    TEST_TIMER_PWM,
    TEST_IIC,
    TEST_SPI,
    TEST_CLK_SET,
    TEST_CHARGE_MODE,
    TEST_PMU_MODE,

    TEST_MODE_MAX,
};

static u8 test_mode = 0;
static u8 charge_current;
static u8 low_power_mode = 0;
static u32 sys_clk_freq = 24000000;


//测试模块初始化
static void mode_init(u8 mode)
{
    switch (mode) {
    case TEST_MCPWM:
        printf("__________**********************\n");
        printf("__________-------test MCPWM-------\n");
        printf("__________**********************\n");
        void mcpwm_test(void);
        mcpwm_test();
        break;
    case TEST_PWM_LED:
        printf("__________**********************\n");
        printf("__________-------test PWM_LED-------\n");
        printf("__________**********************\n");
        void pwm_led_test(void);
        pwm_led_test();
        break;
    case TEST_TIMER_CAP:
        printf("__________**********************\n");
        printf("__________-------test CAP-------\n");
        printf("__________**********************\n");
        void timer_cap_test(void);
        timer_cap_test();
        break;
    case TEST_TIMER_PWM:
        printf("__________**********************\n");
        printf("__________-------test PWM-------\n");
        printf("__________**********************\n");
        void timer_pwm_test(void);
        timer_pwm_test();
        break;
    case TEST_IIC:
        printf("__________**********************\n");
        printf("__________-------test IIC-------\n");
        printf("__________**********************\n");
        void eeprom_test_main();
        eeprom_test_main();
        break;
    case TEST_SPI:
        printf("__________**********************\n");
        printf("__________-------test SPI-------\n");
        printf("__________**********************\n");
        void spi_test_main();
        spi_test_main();
        break;
    case TEST_CLK_SET:
        printf("__________**********************\n");
        printf("__________--------test CLK--------\n");
        printf("__________**********************\n");
        clk_set("sys", sys_clk_freq);
        clk_dump();
        printf("PA_DIR:%x\n", JL_PORTA->DIR);
        printf("CLK_CON0:%x\n", JL_CLOCK->CLK_CON0);
        //输出时钟到PA0
        JL_PORTA->DIR &= ~BIT(0);
        //hsb
        JL_CLOCK->CLK_CON0 |= BIT(12);
        JL_CLOCK->CLK_CON0 &= ~BIT(11);
        JL_CLOCK->CLK_CON0 |= BIT(10);
        break;
    case TEST_CHARGE_MODE:
        printf("__________**********************\n");
        printf("__________--------test CHARGE--------\n");
        printf("__________**********************\n");
        puts("TEST_CHARGE_MODE init\n");
        charge_current = charge_get_mA_config();
        charge_set_mA(charge_current);
        break;

    case TEST_PMU_MODE:
        printf("__________**********************\n");
        printf("__________--------test PMU--------\n");
        printf("__________**********************\n");
        break;
    }
}

//测试模块退出(如果需要退出)
static void mode_exit(u8 mode)
{
    switch (mode) {
    case TEST_MCPWM:
        void mcpwm_close(pwm_ch_num_type pwm_ch);
        mcpwm_close(0);
        mcpwm_close(1);
        mcpwm_close(2);
        mcpwm_close(3);
        mcpwm_close(4);
        mcpwm_close(5);
        break;
    case TEST_PWM_LED:
        void close_pwm_led(void);
        close_pwm_led();
        break;
    case TEST_TIMER_CAP:
        void close_timer_cap(JL_TIMER_TypeDef * JL_TIMERx);
        close_timer_cap(JL_TIMER3);
        break;
    case TEST_TIMER_PWM:
        void close_timer_pwm(JL_TIMER_TypeDef * JL_TIMERx);
        close_timer_pwm(JL_TIMER2);
        close_timer_pwm(JL_TIMER3);
        break;
    case TEST_IIC:
        break;
    case TEST_SPI:
        break;
    case TEST_CLK_SET:
        //关闭时钟输出到IO
        JL_PORTA->DIR |= BIT(0);
        break;
    case TEST_CHARGE_MODE:
        puts("TEST_CHARGE_MODE exit\n");
        charge_set_mA(charge_get_mA_config());
        break;
    case TEST_PMU_MODE:
        break;
    }
}

//测试模块响应key（如有需要）
static void mode_key(u8 mode)
{
    switch (mode) {
    case TEST_IIC:
        break;
    case TEST_SPI:
        break;
    case TEST_CLK_SET:
        u32 pll_clk = clk_set("sys", sys_clk_freq);
        printf("clk test %d %d\n", sys_clk_freq, pll_clk);
        clk_dump();
        sys_clk_freq += 1000000;
        if (sys_clk_freq > 240000000) {
            sys_clk_freq = 24000000;
        }
        break;
    case TEST_CHARGE_MODE:
        charge_current++;
        if (charge_current > CHARGE_mA_220) {
            charge_current = CHARGE_mA_20;
        }
        printf("charge_current:%d\n", charge_current);
        charge_set_mA(charge_current);
        break;

    case TEST_PMU_MODE:
        lowpower_mode(low_power_mode++);
        break;
    }
}


//切换测试模式
static void switch_test_mode()
{
    if (test_mode != 0) {
        mode_exit(test_mode);
    }

    test_mode++;
    if (test_mode == TEST_MODE_MAX) {
        test_mode = 1;
    }

    printf("switch_test_mode %d\n", test_mode);
    mode_init(test_mode);
}

//测试消息响应
void sdk_test(void)
{
    int msg = get_msg();
    if (msg == NO_MSG) {
        return;
    }

    switch (msg) {
    case MSG_TEST_IO_KEY1_SHORT:
        switch_test_mode();
        break;
    case MSG_TEST_IO_KEY1_LONG:
        break;
    case MSG_TEST_IO_KEY1_HOLD:
        break;
    case MSG_TEST_IO_KEY1_LONG_HOLD_UP:
        break;



    case MSG_TEST_IO_KEY2_SHORT:
        puts("mode key\n");
        mode_key(test_mode);
        break;
    case MSG_TEST_IO_KEY2_LONG:
        break;
    case MSG_TEST_IO_KEY2_HOLD:
        break;
    case MSG_TEST_IO_KEY2_LONG_HOLD_UP:
        break;
    default:
        break;
    }
}
#endif
