#include "asm/includes.h"



/*-----------------------------------------------------------*/

#define DEBUG_WR_SFR_EN 	do{JL_DEBUG->WR_EN = 0xE7;}while(0)

#define _DSP_BF_CON				JL_DEBUG->DSP_BF_CON
#define _WR_EN					JL_DEBUG->WR_EN
#define _DEBUG_MSG				JL_DEBUG->MSG
#define _DEBUG_MSG_CLR			JL_DEBUG->MSG_CLR
#define _DSP_PC_LIML0			JL_DEBUG->DSP_PC_LIML0
#define _DSP_PC_LIMH0			JL_DEBUG->DSP_PC_LIMH0
#define _DSP_PC_LIML1			JL_DEBUG->DSP_PC_LIML1
#define _DSP_PC_LIMH1			JL_DEBUG->DSP_PC_LIMH1
#define _DSP_EX_LIML			JL_DEBUG->DSP_EX_LIML
#define _DSP_EX_LIMH			JL_DEBUG->DSP_EX_LIMH
#define _PRP_EX_LIML			JL_DEBUG->PRP_EX_LIML
#define _PRP_EX_LIMH			JL_DEBUG->PRP_EX_LIMH
#define _PRP_MMU_MSG			JL_DEBUG->PRP_MMU_MSG
#define _LSB_MMU_MSG_CH			JL_DEBUG->LSB_MMU_MSG_CH
#define _PRP_WR_LIMIT_MSG		JL_DEBUG->PRP_WR_LIMIT_MSG
#define _LSB_WR_LIMIT_CH		JL_DEBUG->LSB_WR_LIMIT_CH
#define _PRP_SRM_INV_MSG		JL_DEBUG->PRP_SRM_INV_MSG
#define _LSB_SRM_INV_CH        JL_DEBUG->LSB_SRM_INV_CH
#define _DSPCON					JL_DSP->CON
#define _EMU_CON				q32DSP(0)->EMU_CON
#define _EMU_MSG				q32DSP(0)->EMU_MSG
#define _EMU_SSP_H              q32DSP(0)->EMU_SSP_H
#define _EMU_SSP_L              q32DSP(0)->EMU_SSP_L
#define _EMU_USP_H              q32DSP(0)->EMU_USP_H
#define _EMU_USP_L              q32DSP(0)->EMU_USP_L
#define _ETM_CON                q32DSP(0)->ETM_CON


#define debug_log 	log_d

static char *const  debug_msg[32] = {
    "reserved",		    //31
    "access_IcacheRam_or_DcacheRam",    //30
    "reserved",		    //29
    "access_sbcRam_is_not_sbc",		    //28

    "reserved",		    //27
    "cpu_write_reserved_address",	    //26
    "cpu_read_reserved_address",	    //25
    "cpu_instruction_fetch_from_reserved_address",	        //24

    "reserved",         //23
    "reserved",         //22
    "reserved",         //21
    "peripheral_access_reserved_address",		            //20

    "peripheral_write_external_memory_when_cache_disable",	//19
    "peripheral_read_external_memory_when_cache_disable",	//18
    "cpu_write_data_over_limit",	    //17
    "cpu_execute_over_limit",		    //16

    "reserved",         //15
    "reserved",         //14
    "reserved",         //13
    "reserved",         //12

    "reserved",         //11
    "reserved",         //10
    "peripheral_access_mmu_error",	    //9
    "cpu_access_mmu_error",		        //8

    "reserved",         //7
    "reserved",         //6
    "reserved",         //5
    "reserved",         //4

    "reserved",         //3
    "peripheral_write_data_over_limit",	//2
    "reserved",		    //1
    "watchdog",	        //0
};

static char *const prp_mmu_err_msg[32] = {
    "reserved",		//31
    "reserved",		//30
    "reserved",		//29
    "reserved",		//28

    "reserved",		//27
    "reserved",		//26
    "reserved",		//25
    "reserved",		//24

    "reserved",		//23
    "reserved",		//22
    "reserved",		//21
    "reserved",		//20

    "reserved",		//19
    "reserved",		//18
    "reserved",		//17
    "reserved",		//16

    "reserved",		//15
    "reserved",		//14
    "CPU_IF",		//13
    "CPU_RD",		//12

    "CPU_WR",		//11
    "reserved",		//10
    "FIR",		    //9
    "EQ",		    //8

    "FFT",  	    //7
    "BT",		    //6
    "FM",  	        //5
    "LSB_LG1",		//4

    "LSB_LG0",		//3
    "DCP_RD",		//2
    "DCP_WR",	    //1
    "USB",	        //0
};

static char *const prp_err_msg[32] = {
    "reserved",		//31
    "reserved",		//30
    "reserved",		//29
    "reserved",		//28

    "reserved",		//27
    "reserved",		//26
    "reserved",		//25
    "reserved",		//24

    "reserved",		//23
    "reserved",		//22
    "reserved",		//21
    "reserved",		//20

    "reserved",		//19
    "reserved",		//18
    "reserved",		//17
    "DCP_CACHE",    //16

    "DCP_SRM",	    //15
    "reserved",		//14
    "reserved",		//13
    "reserved",		//12

    "reserved",		//11
    "reserved",		//10
    "reserved",		//9
    "FM",		    //8

    "BT",  	        //7
    "reserved",		//6
    "FIR",  	    //5
    "FFT",		    //4

    "EQ",		    //3
    "USB",		    //2
    "LSB_LG1",	    //1
    "LSB_LG0",	    //0
};

static char *const lsb_err_msg[32] = {
    "reserved",		//31
    "reserved",		//30
    "reserved",		//29
    "reserved",		//28

    "reserved",		//27
    "reserved",		//26
    "reserved",		//25
    "reserved",		//24

    "CTMU", 		//23
    "UART2_RD",		//22
    "UART2_WR",		//21
    "UART1_RD",		//20

    "UART1_WR",		//19
    "UART0_RD",		//18
    "UART0_WR",		//17
    "SPI2",		    //16

    "SPI1", 		//15
    "SPI0",		    //14
    "SD1_D",		//13
    "SD1_C",		//12

    "SD0_D",		//11
    "SD0_C",		//10
    "SBC",  		//9
    "PLNK",	    	//8

    "PAP",	    	//7
    "ISP",  		//6
    "SPDIF_I",		//5
    "SPDIF_D",		//4

    "AUDIO",		//3
    "ALNK1",		//2
    "ALNK0",		//1
    "reserved",		//0
};

static char *const emu_msg[32] = {
    "reserved",		//31
    "reserved",		//30
    "reserved",		//29
    "reserved",		//28

    "reserved",		//27
    "reserved",		//26
    "reserved",		//25
    "reserved",		//24

    "reserved",		//23
    "reserved",		//22
    "reserved",		//21
    "fpu_ine_err",	//20

    "fpu_huge_err",	//19
    "fpu_tiny",		//18
    "fpu_inf_err",	//17
    "fpu_inv_err",	//16

    "reserved",		//15
    "reserved",		//14
    "reserved",		//13
    "reserved",		//12

    "reserved",		//11
    "reserved",		//10
    "reserved",		//9
    "reserved",		//8

    "reserved",		//7
    "reserved",		//6
    "reserved",		//5
    "reserved",		//4

    "reserved",		//3
    "div0_err",		//2
    "illeg_err",	//1
    "misalign_err",	//0
};


/********************************* DEBUG PART **********************************/
static void debug_enter_critical()
{
    while (!(JL_DEBUG->WR_EN & BIT(0))) {
        DEBUG_WR_SFR_EN;
    }
}

static void debug_exit_critical()
{
    while (JL_DEBUG->WR_EN & BIT(0)) {
        DEBUG_WR_SFR_EN;
    }
}

static void pc_rang_limit0(void *low_addr, void *high_addr)
{
    debug_enter_critical();
    _DSP_PC_LIML0 = (u32)low_addr;
    _DSP_PC_LIMH0 = (u32)high_addr;
    debug_exit_critical();
}

static void pc_rang_limit1(void *low_addr, void *high_addr)
{
    debug_enter_critical();
    _DSP_PC_LIML1 = (u32)low_addr;
    _DSP_PC_LIMH1 = (u32)high_addr;
    debug_exit_critical();
}

static u32 flag1;
static u32 suspend_cnt1;

void prp_store_rang_limit_set(void *low_addr, void *high_addr, u8 mode)
{
    debug_enter_critical();

    _DSP_BF_CON &= ~BIT(8);

    _PRP_EX_LIML = (u32)low_addr;
    _PRP_EX_LIMH = (u32)high_addr;

    if (mode) {
        _DSP_BF_CON |= BIT(9);  //写框内触发
    } else {
        _DSP_BF_CON &= ~BIT(9); //写框外触发
    }

    _DSP_BF_CON |= BIT(8);

    debug_exit_critical();
}

void prp_protect_ram_suspend(void)
{
    debug_enter_critical();
    //restore sfr setting
    flag1 = _DSP_BF_CON;
    suspend_cnt1++;
    //disable if enable
    if (flag1 & BIT(8)) {
        _DSP_BF_CON &= ~BIT(8);
    }
    /* log_info("flag1 : %x\r\n", flag1); */
    debug_exit_critical();
}

void prp_protect_ram_resume(void)
{
    debug_enter_critical();
    ASSERT(suspend_cnt1, "\r\n----use when no suspend %s %x %x\r\n", __func__, suspend_cnt1, _DSP_BF_CON);

    if (flag1 & BIT(8)) {
        _DSP_BF_CON |= BIT(8);
    }
    /* flag1 = -1; */
    suspend_cnt1--;
    debug_exit_critical();
}

static u32 flag;
static u32 suspend_cnt;

void cpu_store_rang_limit_set(void *low_addr, void *high_addr, u8 inside)
{
    debug_enter_critical();

    _DSP_BF_CON &= ~BIT(10);

    _DSP_EX_LIML = (u32)low_addr;
    _DSP_EX_LIMH = (u32)high_addr;

    if (inside) {
        _DSP_BF_CON |= BIT(11);
    } else {
        _DSP_BF_CON &= ~BIT(11);
    }

    _DSP_BF_CON |= BIT(10);

    debug_exit_critical();
}

void cpu_protect_ram_suspend(void)
{
    debug_enter_critical();
    //restore sfr setting
    flag = _DSP_BF_CON;
    suspend_cnt++;
    //disable if enable
    if (flag & BIT(10)) {
        _DSP_BF_CON &= ~BIT(10);
    }
    /* log_info("flag : %x\r\n", flag); */
    debug_exit_critical();
}

void cpu_protect_ram_resume(void)
{
    debug_enter_critical();
    ASSERT(suspend_cnt, "\r\n----use when no suspend %s %x %x\r\n", __func__, suspend_cnt, _DSP_BF_CON);

    if (flag & BIT(10)) {
        _DSP_BF_CON |= BIT(10);
    }
    /* flag = -1; */
    suspend_cnt--;
    debug_exit_critical();
}

void icache_exception_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(30);
    } else {
        _DSPCON &= ~BIT(30);
    }
    debug_exit_critical();
}
void peripheral_access_MMU_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(29);
    } else {
        _DSPCON &= ~BIT(29);
    }
    debug_exit_critical();
}

void perpheral_access_invalid_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(28);
    } else {
        _DSPCON &= ~BIT(28);
    }
    debug_exit_critical();
}

void cpu_load_ins_invalid_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(27);
    } else {
        _DSPCON &= ~BIT(27);
    }
    debug_exit_critical();
}

void cpu_load_data_invalid_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(26);
    } else {
        _DSPCON &= ~BIT(26);
    }
    debug_exit_critical();
}


void cpu_store_invalid_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(25);
    } else {
        _DSPCON &= ~BIT(25);
    }
    debug_exit_critical();
}

void peripheral_cpu_access_invalid_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _DSPCON |= BIT(24);
    } else {
        _DSPCON &= ~BIT(24);
    }
    debug_exit_critical();
}

/******************************** EMU PART  *****************************/
void emu_misalign_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(0);
    } else {
        _EMU_CON &= ~BIT(0);
    }
    debug_exit_critical();
}


void emu_illeg_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(1);
    } else {
        _EMU_CON &= ~BIT(1);
    }
    debug_exit_critical();
}

void emu_div0_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(2);
    } else {
        _EMU_CON &= ~BIT(2);
    }
    debug_exit_critical();
}

void emu_fpu_inv_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(16);
    } else {
        _EMU_CON &= ~BIT(16);
    }
    debug_exit_critical();
}

void emu_fpu_inf_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(17);
    } else {
        _EMU_CON &= ~BIT(17);
    }
    debug_exit_critical();
}

void emu_fpu_tiny_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(15);
    } else {
        _EMU_CON &= ~BIT(15);
    }
    debug_exit_critical();
}

void emu_fpu_huge_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(19);
    } else {
        _EMU_CON &= ~BIT(19);
    }
    debug_exit_critical();
}

void emu_fpu_ine_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _EMU_CON |= BIT(20);
    } else {
        _EMU_CON &= ~BIT(20);
    }
    debug_exit_critical();
}
void emu_stack_limit_set(u8 mode, u32 limit_l, u32 limit_h)
{
    debug_enter_critical();
    if (mode) {
        _EMU_SSP_H = limit_h;
        _EMU_SSP_L = limit_l;
        /* log_d("SSP_H : 0x%x", _EMU_SSP_H); */
        /* log_d("SSP_L : 0x%x", _EMU_SSP_L); */
    } else {
        _EMU_USP_H = limit_h;
        _EMU_USP_L = limit_l;
        /* log_d("USP_H : 0x%x", _EMU_USP_H); */
        /* log_d("USP_L : 0x%x", _EMU_USP_L); */
    }
    debug_exit_critical();
}

void etm_pc_trace_enable(u8 enable)
{
    debug_enter_critical();
    if (enable) {
        _ETM_CON |= BIT(0);
    } else {
        _ETM_CON &= ~BIT(0);
    }
    debug_exit_critical();
}



/**************************************************/

#define STACK_MAGIC     0x5a5a5a5a
static u32 stack_magic[4];
static u32 stack_magic0[4] ;
#define TRIGGER         __asm__ volatile ("trigger")

u32 data_magic[16] sec(.data_magic) ;


void __attribute__((weak)) exception_analyze_user()
{

}


void __attribute__((weak)) exception_analyze_release()
{
    cpu_reset();
}


void exception_analyze(unsigned int *sp)
{
#ifdef CONFIG_RELEASE_ENABLE
    exception_analyze_release();
    return;
#endif
    /* uart_init_tmp(); */
    /* TRIGGER; */
    unsigned int reti = sp[16];
    unsigned int rete = sp[17];
    unsigned int retx = sp[18];
    unsigned int rets = sp[19];
    unsigned int psr  = sp[20];
    unsigned int icfg = sp[21];
    unsigned int usp  = sp[22];
    unsigned int ssp  = sp[23];

    int i;

    puts("\n\n---------system--exception---------\n");
    printf("PC: 0x%x --> 0x%x --> 0x%x --> 0x%x\n",
           q32DSP(0)->ETM_PC3, q32DSP(0)->ETM_PC2,
           q32DSP(0)->ETM_PC1, q32DSP(0)->ETM_PC0);

    if (stack_magic[0] != STACK_MAGIC) {
        printf("\r\nStack Magic destory! %x != %x", stack_magic[0], STACK_MAGIC);
    }

    if (stack_magic0[0] != STACK_MAGIC) {
        printf("\r\nStack Magic0 destory! %x != %x", stack_magic0[0], STACK_MAGIC);
    }


    printf("usp   : 0x%08x\n", usp);
    printf("ssp   : 0x%08x\n", ssp);
    printf("sp    : 0x%08x\n", (u32)sp);
    printf("rets  : 0x%08x\n", rets);
    printf("reti  : 0x%08x\n", reti);
    printf("retx  : 0x%08x\n", retx);
    printf("rete  : 0x%08x\n", rete);
    printf("psr   : 0x%08x\n", psr);
    printf("icfg  : 0x%08x\n", icfg);




    printf("\n--reti--- \n ");
    put_buf((u8 *)reti - 0x20, 0x40);

    printf("\n--rets--- \n ");
    put_buf((u8 *)rets - 0x20, 0x40);

    printf("\r\nDEBUG_MSG %x", _DEBUG_MSG);
    printf("\r\nDSPCON %x", _DSPCON);
    printf("\r\nEMU_CON %x", _EMU_CON);
    printf("\r\nEMU_MSG %x", _EMU_MSG);
    printf("\r\nPRP_MMU_MSG %x", _PRP_MMU_MSG);
    printf("\r\nLSB_MMU_MSG_CH %x", _LSB_MMU_MSG_CH);
    printf("\r\nPRP_WR_LIMIT_MSG %x", _PRP_WR_LIMIT_MSG);
    printf("\r\nLSB_WR_LIMIT_CH %x", _LSB_WR_LIMIT_CH);
    printf("\r\nPRP_SRM_INV_MSG %x", _PRP_SRM_INV_MSG);
    printf("\r\nLSB_SRM_INV_CH %x", _LSB_SRM_INV_CH);


    for (i = 0; i < 32; i++) {
        if (_DEBUG_MSG & BIT(i)) {
            printf("\r\ndebug err msg : %s", debug_msg[31 - i]);
        }
    }

    for (i = 0; i < 32; i++) {
        if (_EMU_MSG & BIT(i)) {
            printf("\r\nemu err msg : %s", emu_msg[31 - i]);
        }
    }

    if (_DEBUG_MSG & BIT(9)) {
        for (i = 0; i < 32; i++) {
            if (_PRP_MMU_MSG & BIT(i)) {
                printf("\r\nprp wr mmu err msg : %s", prp_mmu_err_msg[31 - i]);
            }
        }
        //lsb group 1&0
        if ((_PRP_MMU_MSG & BIT(3)) || (_PRP_MMU_MSG & BIT(4))) {
            printf("\r\nlsb wr mmu err msg : %s", lsb_err_msg[31 - (_LSB_MMU_MSG_CH & 0xff)]);
        }
    }

    if (_DEBUG_MSG & BIT(2)) {
        for (i = 0; i < 32; i++) {
            if (_PRP_WR_LIMIT_MSG & BIT(i)) {
                printf("\r\nprp wr limit err msg : %s", prp_err_msg[31 - i]);
            }
        }
        //lsb group 1&0
        if ((_PRP_WR_LIMIT_MSG & BIT(1)) || (_PRP_WR_LIMIT_MSG & BIT(0))) {
            printf("\r\nlsb wr limit err msg : %s", lsb_err_msg[31 - (_LSB_WR_LIMIT_CH & 0xff)]);
        }
    }

    for (i = 0; i < 16; ++i) {
        printf("R%d = 0x%08x\n", i, sp[i]);
    }
    for (i = 0; i < ARRAY_SIZE(stack_magic); i++) {
        if (stack_magic[i] != 0x5a5a5a5a) {
            printf("data_magic: %d, 0x%08x\n", i, stack_magic[i]);
        }
    }

    for (i = 0; i < ARRAY_SIZE(stack_magic0); i++) {
        if (stack_magic0[i] != 0x5a5a5a5a) {
            printf("stack_magic0: %d, 0x%08x\n", i, stack_magic0[i]);
        }
    }

    for (i = 0; i < ARRAY_SIZE(data_magic); i++) {
        if (data_magic[i] != 0xffffffff) {
            printf("data_magic: %d, 0x%08x\n", i, data_magic[i]);
        }
    }


    exception_analyze_user();

    while (1);
}



extern int text_begin;
extern int text_end;
extern int _data_code_begin;
extern int _data_code_end;
extern int _cpu_store_begin, _cpu_store_end;
extern int _prp_store_begin, _prp_store_end;
extern int _nv_pre_begin, _nv_pre_end;
extern u32 _cpu0_sstack_begin;
extern u32 _cpu0_sstack_end;

void debug_dump(void)
{
    printf("DSP_CON : 0x%x", _DSPCON);
    printf("EMU_CON : 0x%x", _EMU_CON);
    printf("ETM_CON : 0x%x", _ETM_CON);
    printf("EMU_MSG : 0x%x", _EMU_MSG);
    printf("DEBUG_MSG : 0x%x", _DEBUG_MSG);
    printf("PRP_MMU_MSG : 0x%x", _PRP_MMU_MSG);
    printf("LSB_MMU_MSG_CH : 0x%x", _LSB_MMU_MSG_CH);
    printf("PRP_WR_LIMIT_MSG : 0x%x", _PRP_WR_LIMIT_MSG);
}
void ram_protect_close(void)
{
    debug_enter_critical();
    _DSPCON &= ~(0b1111111 << 24);
    _DSP_BF_CON = 0;
    _EMU_CON = 3;
    pc_rang_limit0((void *)0, (void *)0x1ffffff);
    pc_rang_limit1((void *)0, (void *)0x1ffffff);
    emu_stack_limit_set(0, 0x0, 0x1ffffff);
    emu_stack_limit_set(1, 0x0, 0x1ffffff);
    debug_exit_critical();
    /* perpheral_access_invalid_enable(0); */
    /* peripheral_cpu_access_invalid_enable(0); */
    /* cpu_load_data_invalid_enable(0); */
    /* cpu_store_invalid_enable(0); */
    /* cpu_load_ins_invalid_enable(0); */
    /* icache_exception_enable(0); */
    /* peripheral_access_MMU_enable(0); */
    /* pc_rang_limit0((void *)0, (void *)0x1ffffff); */
    /* cpu_protect_ram_suspend(); */
    /* prp_protect_ram_suspend(); */

    /* log_d("DSP_CON : 0x%x\n", _DSPCON);
    log_d("EMU_CON : 0x%x\n", _EMU_CON);
    log_d("DSP_BF_CON :0x%x\n",_DSP_BF_CON); */
}

void debug_init()
{
    puts(">>> debug init <<< \n");

    //DEBUG
    peripheral_access_MMU_enable(1);
    perpheral_access_invalid_enable(1);
    cpu_load_ins_invalid_enable(1);
    cpu_load_data_invalid_enable(1);
    cpu_store_invalid_enable(1);
    peripheral_cpu_access_invalid_enable(1);

    //EMU
    emu_misalign_enable(1);
    emu_illeg_enable(1);
    emu_div0_enable(1);

    // !!! br23/br25不能打开浮点异常使能 !!!

    //ETM
    etm_pc_trace_enable(1);

    //CPU execute protect
    pc_rang_limit0(&text_begin, &text_end);
    /* pc_rang_limit1(0, (u8 *)0x030000); */
    /* pc_rang_limit1(&_data_code_begin, &_data_code_end); */
    printf("pc_limit0: 0x%x ~ 0x%x\n",  _DSP_PC_LIML0 & 0x1ffffff, _DSP_PC_LIMH0 & 0x1ffffff);
    printf("pc_limit1: 0x%x ~ 0x%x\n",  _DSP_PC_LIML1 & 0x1ffffff, _DSP_PC_LIMH1 & 0x1ffffff);

    /* u32 nv_pre_len = (u32)&_nv_pre_end - (u32)&_nv_pre_begin; */
    /* memset((u8 *)&_nv_pre_begin, 0xff, nv_pre_len); */
    /* put_buf((u8 *)&_nv_pre_begin, nv_pre_len); */

    memset(stack_magic, 0x5a, sizeof(stack_magic));
    memset(stack_magic0, 0x5a, sizeof(stack_magic0));
    memset(data_magic, 0xff, sizeof(data_magic));

    /* log_d("CPU STORE  Limit0: 0x%x ~ 0x%x", &_cpu_store_begin, &_cpu_store_end); */
    /* log_d("PRP STORE  Limit0: 0x%x ~ 0x%x", &_prp_store_begin, &_prp_store_end); */

    //cpu store protect
    cpu_store_rang_limit_set((void *)&_data_code_begin, (void *)&_data_code_end - 1, 1);
    /* cpu_store_rang_limit_set((void *)&_nv_pre_begin, (void *)&_nv_pre_end - 1, 1); */
    /* cpu_store_rang_limit_set((void *)&_cpu_store_begin, (void *)&_cpu_store_end - 1, 0); */
    /* cpu_store_rang_limit_set((void *)&_cpu_store_begin, (void *)&_cpu_store_end - 1, 1); */
    /* cpu_store_rang_limit_set((void *)0x2bf00, (void *)(0x2bf00 + 4), 1); */
    /* cpu_store_rang_limit_set((void *)stack_magic, (void *)stack_magic + 16 - 1, 1); */
    /* cpu_store_rang_limit_set((void *)0x0, (u8 *)(sizeof(data_magic) - 1), 1); */

    //prp store protect
    prp_store_rang_limit_set((void *)&_data_code_begin, (void *)&_data_code_end - 1, 1);
    /* prp_store_rang_limit_set((void *)&_nv_pre_begin, (void *)&_nv_pre_end - 1, 1); */
    /* prp_store_rang_limit_set((void *)0x0, (void *)0x18000-1, 0); */
    /* prp_store_rang_limit_set((void *)&_prp_store_begin, (void *)&_prp_store_end - 1, 1); */
    /* prp_store_rang_limit_set((void *)0x0, (u8 *)(sizeof(data_magic) - 1), 1); */

    emu_stack_limit_set(1, (u32)&_cpu0_sstack_begin, (u32)&_cpu0_sstack_end);

    /* log_d("CPU STORE Limit1: 0x%x ~ 0x%x", _DSP_EX_LIML, _DSP_EX_LIMH); */
    /* log_d("PRP Limit1: 0x%x ~ 0x%x", _PRP_EX_LIML, _PRP_EX_LIMH); */

#if 0
    q32DSP(0)->EMU_CON &= ~ BIT(3);
    q32DSP(0)->EMU_SSP_L = (int)&_cpu0_sstack_begin;
    q32DSP(0)->EMU_SSP_H = (int)&_cpu0_sstack_end;
    q32DSP(0)->EMU_CON |= BIT(3);
#endif
    /* q32DSP(0)->EMU_USP_L = 0;//(int)cpu0_sstack_begin; */
    /* q32DSP(0)->EMU_USP_H = -1;//(int)cpu0_sstack_end;  */
    /* q32DSP(0)->ETM_CON |= BIT(0); */
    /* log_d("cpu 0 usp limit %x  %x\r\n", q32DSP(0)->EMU_USP_L, q32DSP(0)->EMU_USP_H); */
    /* log_d("cpu 0 ssp limit %x  %x\r\n", q32DSP(0)->EMU_SSP_L, q32DSP(0)->EMU_SSP_H); */

    //error sample
    /* exception_cpu_ex_over_limit_error(); */
    /* exception_cpu_store_error(); */
    /* debug_sfr_test(); */

    /* debug_dump(); */
}


void debug_disable()
{
    _EMU_CON = 0;
}



