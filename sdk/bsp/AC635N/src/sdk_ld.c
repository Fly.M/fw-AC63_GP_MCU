// *INDENT-OFF*
/* #include "app_config.h" */

/*
|_______________|
|    ram1       |
|_______________|
|    TLB        |
|_______________|
|   isr base    |   (HW fixed)
|_______________|
|    ram0       |
|_______________|
|    DCACHE     |
|_______________|
*/

RAM1_LIMIT_L    = 0x2C000;
RAM1_LIMIT_H    = 0x30000;


/********************************************/
/*           0x2FF80 ~ 0x30000              */
/********************************************/

RAM1_BEGIN      = RAM1_LIMIT_L;
RAM1_END        = RAM1_LIMIT_H;
RAM1_SIZE       = RAM1_END - RAM1_BEGIN;


RAM_LIMIT_L     = 0x00000;
RAM_LIMIT_H     = 0x2C000;
/********************************************/
/*           0x2BF00 ~ 0x2C000              */
/********************************************/
/* ISR_BASE        = _IRQ_MEM_ADDR; */
ISR_SIZE        = 0x100;
ISR_BASE        = RAM_LIMIT_H - ISR_SIZE;

/********************************************/
/*           0x00000 ~ 0x2BF00              */
/********************************************/
RAM_BEGIN       = RAM_LIMIT_L;
RAM_END         = RAM_LIMIT_H - ISR_SIZE;
RAM_SIZE        = RAM_END - RAM_BEGIN;



CODE_BEG   = 0X1E000C0;

UPDATA_BREDR_BASE_BEG = 0xF9000;


MEMORY
{
	code0(rx)    	  : ORIGIN =  0x1E00100,   LENGTH = 1024*1024
	ram0(rwx)         : ORIGIN =  RAM_BEGIN  , LENGTH = RAM_SIZE
    ram1(rwx)         : ORIGIN =  RAM1_BEGIN , LENGTH = RAM1_SIZE
}

#include "maskrom_stubs.ld"

SECTIONS
{
/********************************************/
    . = ORIGIN(code0);
    .text ALIGN(4):
    {
        text_code_begin = .;

        PROVIDE(text_rodata_begin = .);

        *(.startup.text)

        *(.mem_code)
        *(.mem_const)
		. = ALIGN(4);

        clock_critical_handler_begin = .;
        KEEP(*(.clock_critical_txt))
        clock_critical_handler_end = .;

        . = ALIGN(4);
        __VERSION_BEGIN = .;
        KEEP(*(.sys.version))
        __VERSION_END = .;

        *(.noop_version)

		. = ALIGN(4);


		. = ALIGN(4);
        timer_target_begin = .;
        KEEP(*(.timer_target))
        timer_target_end = .;

        CLOCK_CODE_START = .;
        *(.clock_code)
        *(.clock_const)
        CLOCK_CODE_SIZE = ABSOLUTE(. - CLOCK_CODE_START);

        *(.power_code)
        *(.power_const)

        *(.text*)
        *(.LOG_TAG_CONST*)
        *(.rodata*)
		. = ALIGN(4);

        text_code_end = .;

    } >code0


    . = ORIGIN(ram0);


        //cpu start
   	.data ALIGN(4):
	  {
		/// 放在data 里面的code 必须放在这个位置保护起来

        *(.data_magic)
        . = ALIGN(4);

        *(.flushinv_icache)
        *(.volatile_ram_code)
        *(.os_critical_code)

        _data_code_begin = .;
		*(.bank_critical_code)
        . = ALIGN(4);

		*(.flushinv_icache)
        *(.cache)
        *(.volatile_ram_code)
		*(.non_volatile_ram_code)
        _SPI_CODE_START = . ;
        *(.spi_code)
        _SPI_CODE_END = . ;
		. = ALIGN(4);

		. = ALIGN(4);
		lp_target_begin = .;
		*(.lp_target)
		lp_target_end = .;
		. = ALIGN(4);

        *(.gpio_code)
        *(.os_port_code)

        _data_code_end = .;


    	_cpu_store_begin = . ;
		. = ALIGN(4);
        *(.data*)
        *(.clock_data)
        *(.mem_data)

        *(.mem_bss)
		*(.non_volatile_ram)
		. = ALIGN(4);
	  } > ram0

    .irq_stack ALIGN(32) :
    {
		*(.stack_magic)
        _cpu0_sstack_begin = .;
        PROVIDE(cpu0_sstack_begin = .);
        *(.stack)
        _cpu0_sstack_end = .;
        PROVIDE(cpu0_sstack_end = .);
    	_stack_end = . ;
		*(.stack_magic0)

		. = ALIGN(4);
		*(.boot_info)

    } > ram0

    .bss ALIGN(32) :
    {
        *(.usb_h_dma)   //由于usb有个bug，会导致dma写的数据超出预设的buf，最长可能写超1k，为了避免死机，所以usb dma buffer后面放一些其他模块的buff来避免死机
        *(.usb_ep0)
        *(.sd0_var)
        *(.sd1_var)

        *(.bss)
        *(COMMON)
        /* *(.mem_bss) */

        *(.volatile_ram)

		. = (( . + 31) / 32 * 32);

		. = ALIGN(4);
    } > ram0

	.vir_timer ALIGN(32):
    {
        *(.vir_rtc)
	} > ram0

    //cpu end
    _cpu_store_end = . ;

    _prp_store_begin = . ;
    .prp_bss ALIGN(32) :
    {
        //bt
        //
        //sbc
        //
        //audio
		. = (( . + 31) / 32 * 32);
    } > ram0
    _prp_store_end = . ;

    .bss ALIGN(32) :
	{
		NVRAM_DATA_START = .;
		/* *(.non_volatile_ram) */
		NVRAM_DATA_SIZE = ABSOLUTE(. - NVRAM_DATA_START);
		. = ALIGN(4);
		NVRAM_END = .;
		_nv_pre_begin = . ;

		. = ALIGN(4);

    } > ram0

    RAM_USED = .;

	. =ORIGIN(ram1);
    //TLB 起始需要16K 对齐；
    .mmu_tlb ALIGN(0x4000):
    {
        *(.mmu_tlb_segment);
    } > ram1


    .bss1 ALIGN(32) :
	{
    } > ram1
    RAM1_USED = .;
}

text_begin  = ADDR(.text) ;
text_size   = SIZEOF(.text) ;
text_end    = ADDR(.text) + SIZEOF(.text) ;

bss_begin = ADDR(.bss) ;
bss_size  = SIZEOF(.bss);

bss1_begin = ADDR(.bss1) ;
bss1_size  = SIZEOF(.bss1);
data_addr = ADDR(.data)  ;
data_begin = text_begin + text_size;
data_size =  SIZEOF(.data) ;




_HEAP_BEGIN = RAM_USED;
PROVIDE(HEAP_BEGIN = RAM_USED);

_HEAP_END = RAM_END;
PROVIDE(HEAP_END = RAM_END);

_HEAP_SIZE = HEAP_END - HEAP_BEGIN;
PROVIDE(HEAP_SIZE = HEAP_END - HEAP_BEGIN);

_HEAP1_BEGIN = RAM1_USED;
PROVIDE(HEAP1_BEGIN = RAM1_USED);

_HEAP1_END = RAM1_END;
PROVIDE(HEAP1_END = RAM1_END);

_HEAP1_SIZE = HEAP1_END - HEAP1_BEGIN;
PROVIDE(HEAP1_SIZE = HEAP1_END - HEAP1_BEGIN);

_MALLOC_SIZE = HEAP_SIZE + HEAP1_SIZE;
PROVIDE(MALLOC_SIZE = HEAP_SIZE + HEAP1_SIZE);


