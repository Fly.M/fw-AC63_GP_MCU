#ifndef _IIC_HW_H_
#define _IIC_HW_H_

#include "typedef.h"

#define IIC_HW_NUM                  1
#define IIC_PORT_GROUP_NUM          4

#define iic_enable(reg)             (reg->CON0 |= BIT(0))
#define iic_disable(reg)            (reg->CON0 &= ~BIT(0))
#define iic_role_host(reg)          (reg->CON0 &= ~BIT(1))
#define iic_role_slave(reg)         (reg->CON0 |= BIT(1))
#define iic_cfg_done(reg)           (reg->CON0 |= BIT(2))
#define iic_dir_out(reg)            (reg->CON0 &= ~BIT(3))
#define iic_dir_in(reg)             (reg->CON0 |= BIT(3))
#define iic_preset_end(reg)         (reg->CON0 |= BIT(4))
#define iic_preset_restart(reg)     (reg->CON0 |= BIT(5))
#define iic_recv_ack(reg)           (reg->CON0 &= ~BIT(6))
#define iic_recv_nack(reg)          (reg->CON0 |= BIT(6))
#define iic_send_is_ack(reg)        (!(reg->CON0 & BIT(7)))
#define iic_isel_direct(reg)        (reg->CON0 &= ~BIT(9))
#define iic_isel_filter(reg)        (reg->CON0 |= BIT(9))
#define iic_si_mode_en(reg)         (reg->CON1 |= BIT(13))
#define iic_si_mode_dis(reg)        (reg->CON1 &= ~BIT(13))

#define iic_set_ie(reg)             (reg->CON0 |= BIT(8))
#define iic_clr_ie(reg)             (reg->CON0 &= ~BIT(8))
#define iic_pnd(reg)                (reg->CON0 & BIT(15))
#define iic_pnd_clr(reg)            (reg->CON0 |= BIT(14))

#define iic_set_end_ie(reg)         (reg->CON0 |= BIT(10))
#define iic_clr_end_ie(reg)         (reg->CON0 &= ~BIT(10))
#define iic_end_pnd(reg)            (reg->CON0 & BIT(13))
#define iic_end_pnd_clr(reg)        (reg->CON0 |= BIT(12))

#define iic_start_pnd(reg)          (reg->CON1 & BIT(15))
#define iic_start_pnd_clr(reg)      (reg->CON1 |= BIT(14))

#define iic_baud_reg(reg)           (reg->BAUD)
#define iic_buf_reg(reg)            (reg->BUF)


typedef const int hw_iic_dev;

enum {IIC_MASTER, IIC_SLAVE};

struct hw_iic_config {
    u8 port;   //example: 'A', 'B', 'C', 'D'
    u32 baudrate;
    u8 hdrive;
    u8 io_filter;
    u8 io_pu;
    u8 role;
};

extern const struct hw_iic_config hw_iic_cfg[];

/*
 * @brief 初始化硬件iic
 * @parm iic  iic句柄
 * @return 0 成功，< 0 失败
 */
int hw_iic_init(hw_iic_dev iic);
/*
 * @brief 关闭硬件iic
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_uninit(hw_iic_dev iic);
/*
 * @brief 挂起硬件iic
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_suspend(hw_iic_dev iic);
/*
 * @brief 恢复硬件iic
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_resume(hw_iic_dev iic);
/*
 * @brief 发送iic起始位
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_start(hw_iic_dev iic);
/*
 * @brief 发送iic结束位
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_stop(hw_iic_dev iic);
/*
 * @brief 发送1个字节
 * @parm iic  iic句柄
 * @parm byte  发送的字节
 * @return 1 收到应答，0 未收到应答
 */
u8 hw_iic_tx_byte(hw_iic_dev iic, u8 byte);
/*
 * @brief 接收1个字节
 * @parm iic  iic句柄
 * @parm ack  1 接收字节后回复应答，0不回复应答
 * @return 接收的字节
 */
u8 hw_iic_rx_byte(hw_iic_dev iic, u8 ack);
/*
 * @brief 接收len个字节
 * @parm iic  iic句柄
 * @parm buf  接收缓冲区基地址
 * @parm len  期望接收长度
 * @return 实际接收长度，< 0表示失败
 */
int hw_iic_read_buf(hw_iic_dev iic, void *buf, int len);
/*
 * @brief 发送len个字节
 * @parm iic  iic句柄
 * @parm buf  发送缓冲区基地址
 * @parm len  期望发送长度
 * @return 实际发送长度，< 0表示失败
 */
int hw_iic_write_buf(hw_iic_dev iic, const void *buf, int len);
/*
 * @brief 设置波特率
 * @parm iic  iic句柄
 * @parm baud  波特率
 * @return 0 成功，< 0 失败
 */
int hw_iic_set_baud(hw_iic_dev iic, u32 baud);
/*
 * @brief 中断使能
 * @parm iic  iic句柄
 * @parm en  1 使能，0 失能
 * @return null
 */
void hw_iic_set_ie(hw_iic_dev iic, u8 en);
/*
 * @brief 判断中断标志
 * @parm iic  iic句柄
 * @return 0 无png / 1 有png
 */
u8 hw_iic_get_pnd(hw_iic_dev iic);
/*
 * @brief 清除中断标志
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_clr_pnd(hw_iic_dev iic);
/*
 * @brief 结束位中断使能
 * @parm iic  iic句柄
 * @parm en  1 使能，0 失能
 * @return null
 */
void hw_iic_set_end_ie(hw_iic_dev iic, u8 en);
/*
 * @brief 判断结束位中断标志
 * @parm iic  iic句柄
 * @return 0 无end png / 1 有end png
 */
u8 hw_iic_get_end_pnd(hw_iic_dev iic);
/*
 * @brief 清除结束位中断标志
 * @parm iic  iic句柄
 * @return null
 */
void hw_iic_clr_end_pnd(hw_iic_dev iic);
/*
 * @brief 设置从机地址
 * @parm iic  iic句柄
 * @parm addr  从机地址高7位有效
 * @parm addr_ack  当地址匹配时自动应答 1:自动应答，0:不应答
 * @return null
 */
void hw_iic_slave_set_addr(hw_iic_dev iic, u8 addr, u8 addr_ack);
/*
 * @brief 从机接收准备
 * @parm iic  iic句柄
 * @parm ack  1 接收字节后回复应答，0不回复应答
 * @return null
 */
void hw_iic_slave_rx_prepare(hw_iic_dev iic, u8 ack);
/*
 * @brief 从机接收
 * @parm iic  iic句柄
 * @parm is_start_addr  返回起始地址信息，若is_start_addr为非空指针，1 收到起始地址，0 失败，若为空指针，忽略
 * @return 接收到的字节
 */
u8 hw_iic_slave_rx_byte(hw_iic_dev iic, bool *is_start_addr);
/*
 * @brief 从机发送
 * @parm iic  iic句柄
 * @parm byte 发送的字节
 * @return null
 */
void hw_iic_slave_tx_byte(hw_iic_dev iic, u8 byte);
/*
 * @brief 从机发送后获取应答位
 * @parm iic  iic句柄
 * @return 应答位 1;收到应答  0:无应答
 */
u8 hw_iic_slave_tx_check_ack(hw_iic_dev iic);

#endif


