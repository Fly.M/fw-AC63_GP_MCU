#ifndef _CPU_CLOCK_
#define _CPU_CLOCK_

#include "typedef.h"

#include "clock_hw.h"

#define    SYS_CLOCK_INPUT_RC       0
#define    SYS_CLOCK_INPUT_BT_OSC   1          //BTOSC 双脚(12-26M)
#define    SYS_CLOCK_INPUT_RTOSCH   2
#define    SYS_CLOCK_INPUT_RTOSCL   3
#define    SYS_CLOCK_INPUT_PAT      4

///衍生时钟源作系统时钟源
#define    SYS_CLOCK_INPUT_PLL_RCL    5
#define    SYS_CLOCK_INPUT_PLL_RCH    6
#define    SYS_CLOCK_INPUT_PLL_BT_OSC  7
#define    SYS_CLOCK_INPUT_PLL_RTOSCH  8
#define    SYS_CLOCK_INPUT_PLL_PAT     9

typedef enum {
    SYS_ICLOCK_INPUT_BTOSC,          //BTOSC 双脚(12-26M)
    SYS_ICLOCK_INPUT_RTOSCH,
    SYS_ICLOCK_INPUT_RTOSCL,
    SYS_ICLOCK_INPUT_PAT,
} SYS_ICLOCK_INPUT;

typedef enum {
    PA0_CLOCK_OUTPUT = 0,
    PA0_CLOCK_OUT_BT_OSC,
    PA0_CLOCK_OUT_RTOSCH,
    PA0_CLOCK_OUT_NULL,

    PA0_CLOCK_OUT_LSB = 4,
    PA0_CLOCK_OUT_HSB,
    PA0_CLOCK_OUT_SFC,
    PA0_CLOCK_OUT_PLL,
} PA0_CLK_OUT;

typedef enum {
    PB8_CLOCK_OUTPUT = 0,
    PB8_CLOCK_OUT_RC,
    PB8_CLOCK_OUT_LRC,
    PB8_CLOCK_OUT_NULL,

    PB8_CLOCK_OUT_PLL75M = 4,
    PB8_CLOCK_OUT_XOSC_FSCK,
    PB8_CLOCK_OUT_PLL320,
    PB8_CLOCK_OUT_PLL107,
} PB8_CLK_OUT;

typedef enum {
    CLK_DIV_1,
    CLK_DIV_4,
    CLK_DIV_16,
    CLK_DIV_64,
    CLK_DIV_2,
    CLK_DIV_8,
    CLK_DIV_32,
    CLK_DIV_128,
    CLK_DIV_256,
    CLK_DIV_1024,
    CLK_DIV_4096,
    CLK_DIV_16384,
    CLK_DIV_512,
    CLK_DIV_2048,
    CLK_DIV_8192,
    CLK_DIV_32768,
} CLK_DIV_4bit;
/*
 * system enter critical and exit critical handle
 * */
struct clock_critical_handler {
    void (*enter)();
    void (*exit)();
};

#define CLOCK_CRITICAL_HANDLE_REG(name, enter, exit) \
	const struct clock_critical_handler clock_##name \
		 SEC_USED(.clock_critical_txt) = {enter, exit};

extern struct clock_critical_handler clock_critical_handler_begin[];
extern struct clock_critical_handler clock_critical_handler_end[];

#define list_for_each_loop_clock_critical(h) \
	for (h=clock_critical_handler_begin; h<clock_critical_handler_end; h++)


int clk_early_init(u8 sys_in, u32 input_freq, u32 out_freq);

int clk_get(const char *name);


/**
 * @brief clk_set
 *
 * @param name "sys"
 * @param clk 24 32 40 48 60 64 80 96 120 137 160 192 240 * Mhz
 *
 * @return
 */
int clk_set(const char *name, int clk);

int clk_set_sys_lock(int clk, int lock_en);

/**
 * @brief clk_dump 打印时钟配置信息
 */
void clk_dump(void);


enum clk_mode {
    CLOCK_MODE_ADAPTIVE = 0,
    CLOCK_MODE_USR,
};


void clk_voltage_init(u8 mode, u8 sys_dvdd, u8 pwr_mode, u8 vdc13);


void clock_reset_lsb_max_freq(u32 max_freq);
/**
 * @brief clock_set_sfc_max_freq
 * 使用前需要保证所使用的flash支持4bit 100Mhz 模式
 *
 * @param dual_max_freq for cmd 3BH BBH
 * @param quad_max_freq for cmd 6BH EBH
 */
void clock_set_sfc_max_freq(u32 dual_max_freq, u32 quad_max_freq);
#endif

