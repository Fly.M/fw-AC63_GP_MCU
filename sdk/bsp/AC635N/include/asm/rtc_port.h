#ifndef __RTC_PORT_H__
#define __RTC_PORT_H__

#include "typedef.h"
#include "asm/gpio.h"

#define IO_PORT_PR_00               (IO_MAX_NUM + 0)
#define IO_PORT_PR_01               (IO_MAX_NUM + 1)
#define IO_PORT_PR_02               (IO_MAX_NUM + 2)
#define IO_PORT_PR_03               (IO_MAX_NUM + 3)
#define IO_PORT_PR_04               (IO_MAX_NUM + 4)

#define GPIOR                       (IO_MAX_NUM)
#define GPIOUSB                     (IO_MAX_NUM + USB_IO_OFFSET)
#define GPIOP33                     (IO_MAX_NUM + P33_IO_OFFSET)

int rtc_port_pr_in(u8 port);

int rtc_port_pr_read(u8 port);

int rtc_port_pr_out(u8 port, bool on);

int rtc_port_pr_hd(u8 port, bool on);

int rtc_port_pr_pu(u8 port, bool on);

int rtc_port_pr_pd(u8 port, bool on);

int rtc_port_pr_die(u8 port, bool on);

int rtc_port_pr_wkup_en_port(u8 port, bool en);

int rtc_port_pr_wkup_edge(u8 port, bool up_down);

int rtc_port_pr_wkup_clear_pnd(u8 port);

int __rtc_port_pr_wkup_clear_pnd();
#endif // __RTC_API_H__
