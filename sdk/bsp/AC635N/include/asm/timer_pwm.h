#ifndef __TIMER_PWM_H__
#define __TIMER_PWM_H__

#include "typedef.h"
#include "asm/includes.h"


/**
 * @param JL_TIMERx : JL_TIMER0/1/2/3/4/5
 * @param fre : 频率，单位Hz
 * @param duty : 初始占空比，0~10000对应0~100%
 * @param pwm_io : JL_PORTA_01, JL_PORTB_02,,,等等，建议填硬件IO
 * @param output_ch : 映射通道，当pwm脚选择非硬件脚时有效，这时我们给他分配output_channel 0/1/2
 */
void timer_pwm_init(JL_TIMER_TypeDef *JL_TIMERx, u32 fre, u32 duty, u32 pwm_io, u8 output_ch);

/**
 * @brief timer_pwm改变占空比函数
 * @param JL_TIMERx : JL_TIMER0/1/2/3
 * @param duty : 占空比，0~10000对应0~100%
 */
void set_timer_pwm_duty(JL_TIMER_TypeDef *JL_TIMERx, u32 duty);

/**
 * @param JL_TIMERx : JL_TIMER0/1/2/3
 */
void close_timer_pwm(JL_TIMER_TypeDef *JL_TIMERx);

/**
 * @brief 参考示例
 */
void timer_pwm_test(void);


#endif


