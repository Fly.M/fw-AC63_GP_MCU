#ifndef _CHARGE_H_
#define _CHARGE_H_

#include "typedef.h"

/*充满电电压选择 3.869V-4.567V*/
#define CHARGE_FULL_V_3869		0
#define CHARGE_FULL_V_3907		1
#define CHARGE_FULL_V_3946		2
#define CHARGE_FULL_V_3985		3
#define CHARGE_FULL_V_4026		4
#define CHARGE_FULL_V_4068		5
#define CHARGE_FULL_V_4122		6
#define CHARGE_FULL_V_4157		7
#define CHARGE_FULL_V_4202		8
#define CHARGE_FULL_V_4249		9
#define CHARGE_FULL_V_4295		10
#define CHARGE_FULL_V_4350		11
#define CHARGE_FULL_V_4398		12
#define CHARGE_FULL_V_4452		13
#define CHARGE_FULL_V_4509		14
#define CHARGE_FULL_V_4567		15
#define CHARGE_FULL_V_MAX       16
/*****************************************/

/*充满电电流选择 2mA-30mA*/
#define CHARGE_FULL_mA_2		0
#define CHARGE_FULL_mA_5		1
#define CHARGE_FULL_mA_7	 	2
#define CHARGE_FULL_mA_10		3
#define CHARGE_FULL_mA_15		4
#define CHARGE_FULL_mA_20		5
#define CHARGE_FULL_mA_25		6
#define CHARGE_FULL_mA_30		7

/*恒流充电电流选择 20-320mA*/
#define CHARGE_mA_20			0
#define CHARGE_mA_40			1
#define CHARGE_mA_60			2
#define CHARGE_mA_80			3
#define CHARGE_mA_100			4
#define CHARGE_mA_120			5
#define CHARGE_mA_140			6
#define CHARGE_mA_160			7
#define CHARGE_mA_180			8
#define CHARGE_mA_200			9
#define CHARGE_mA_220			10
#define CHARGE_mA_240			11
#define CHARGE_mA_260			12
#define CHARGE_mA_280			13
#define CHARGE_mA_300			14
#define CHARGE_mA_320			15

#define CHARGE_CCVOL_V			300		//最低充电电流档转向用户设置充电电流档的电压转换点(AC693X无涓流充电，电池电压低时采用最低电流档，电池电压大于设置的值时采用用户设置的充电电流档)

enum {
    CHARGE_FULL_33V = 0,	//充满标记位
    TERMA_33V,				//模拟测试信号
    VBGOK_33V,				//模拟测试信号
    CICHARGE_33V,			//涓流转恒流信号
};

struct charge_platform_data {
    u8 charge_en;	        //内置充电使能
    u8 charge_poweron_en;	//开机充电使能
    u8 charge_full_V;	    //充满电电压大小
    u8 charge_full_mA;	    //充满电电流大小
    u8 charge_mA;	        //充电电流大小
    u16 vpwr_off_filter;	//vpwr<0.6 拔出过滤值，过滤时间 = (filter*2)ms
    u16 vpwr_on_filter;     //vpwr>vbat 插入过滤值,电压的过滤时间 = (filter*2)ms
    u16 vpwr_keep_filter;   //0.6V<vpwr<vbat 维持电压过滤值,过滤时间= (filter*2)ms
    u16 charge_full_filter; //充满过滤值,连续检测充满信号恒为1才认为充满,过滤时间 = (filter*2)ms
};

/*----------------------------------------------------*/
/**@brief    获取VPWR在线状态
   @param    无
   @return   0:不在线 1:在线
   @note     VPWR电压大于插入电压（约0.6V）则返回值为1
*/
/*----------------------------------------------------*/
extern u8 charge_get_online_flag(void);
/*----------------------------------------------------*/
/**@brief    开始充电
   @param    无
   @return   无
   @note     当VPWR电压大于电池电压时,可进行充电
*/
/*----------------------------------------------------*/
extern void charge_start(void);
/*----------------------------------------------------*/
/**@brief    停止充电
   @param    无
   @return   无
   @note     当VPWR电压小于于电池电压时,可停止充电
*/
/*----------------------------------------------------*/
extern void charge_close(void);
/*----------------------------------------------------*/
/**@brief    获取配置的恒流充电电流档位
   @param    无
   @return   参考charge.h充电电流档位
   @note     该接口用于获取配置的档位
*/
/*----------------------------------------------------*/
extern u8 charge_get_mA_config(void);
/*----------------------------------------------------*/
/**@brief    设置恒流充电电流档位
   @param    参考charge.h充电电流档位
   @return   无
   @note     该接口用于改变充电电流档位,使用场景有:
             1、电池电压小时(<3V),用小电流充电(20mA)
                电池电压大时(>3V),用配置的电流充电(charge_mA)
             2、温度升高时,降低充电电流,减小发热
*/
/*----------------------------------------------------*/
extern void charge_set_mA(u8 charge_mA);


/*----------------------------------------------------*/
/**@brief    获取VPWR和插入电压（约0.6V）比的大小
   @param    无
   @return   0:VPWR<0.6 1:VPWR>0.6
   @note     该接口直接读取寄存器,没有经过软件滤波
*/
/*----------------------------------------------------*/
extern u8 charge_get_vpwr_online_hw(void);
/*----------------------------------------------------*/
/**@brief    获取VPWR和VBAT比的大小
   @param    无
   @return   0:VPWR<VBAT 1:VPWR>VBAT
   @note     该接口直接读取寄存器,没有经过软件滤波
*/
/*----------------------------------------------------*/
extern u8 charge_get_lvcmp_det(void);
/*----------------------------------------------------*/
/**@brief    内置充电初始化
   @param    配置结构体
   @return   无
   @note     设置充电相关寄存器
*/
/*----------------------------------------------------*/
extern void charge_init(void *arg);


#endif    //_CHARGE_H_
