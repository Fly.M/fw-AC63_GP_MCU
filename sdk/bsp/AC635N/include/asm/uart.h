#ifndef ASM_UART_H
#define ASM_UART_H

#include "typedef.h"
#include "gpio.h"

#define UART_TEST_EN 1
// uart0
// IO_PORTA_05, IO_PORTA_06, //tx rx
// IO_PORTB_04, IO_PORTB_06,
// IO_PORTB_05, IO_PORTB_05,
// IO_PORTA_11, IO_PORTA_12

//uart1
// IO_PORTB_00, IO_PORTB_01, //tx rx
// IO_PORTC_00, IO_PORTC_01,
// IO_PORTA_00, IO_PORTA_01,
// IO_PORT_DP, IO_PORT_DM

//uart2
// IO_PORTA_02, IO_PORTA_03, //tx rx
// IO_PORTA_09, IO_PORTA_10,
// IO_PORTB_09, IO_PORTB_10,
// IO_PORTC_04, IO_PORTC_05

struct uart_platform_data {
    u8  id;             //串口ID号, 0xff表示自动分配，如果通道与uart不匹配会选择通道0完成初始化，且不会检查通道是否被占用
    u8  tx_pin;         //发送IO, 0xff表示不使能发送。不是ABCD四组IO，会根据tx_ch_sel打开通道
    u8  tx_ch_sel;      //发送IO通道选择,选择enum OUTPUT_CH_SEL，不使用时赋值0xff
    u8  rx_pin;         //接收IO, 0xff表示不使能接收。不是ABCD四组IO，会根据rx_ch_sel打开通道
    u8  rx_ch_sel;      //接收IO通道选择,取值范围0,1,2,3对应输入通道0~3
    u32 baudrate;       //波特率
};

/*
 * 调试串口初始化, id无效, 接收不支持DMA, 只能按byte接收
 */
extern int debug_uart_init(const struct uart_platform_data *data);


/*
 *串口初始化, 支持DMA接收, 返回串口ID号
 */
extern int uart_init(const struct uart_platform_data *);

/*
 * 设置接收中断处理函数, 此函数需要尽快将数据移走, 否则可能会挡住下次接收
 */
extern void uart_set_rx_irq_handler(int id, void (*handler)(u8 *, u16));

/*
 * 发送一个byte
 */
extern void uart_tx_byte(int id, u8 byte);

/*
 * 同步方式DMA发送批量数据
 */
extern void uart_tx_buf(int id, u8 *txdata, u16 len);

/*
 * 异步方式DMA发送批量数据, 发送完成后回调callback函数
 */
extern void uart_async_tx_buf(int id, u8 *txdata, u16 len, void (*callback)());

/*
 * 关闭串口，返回0成功或1失败
 */
u32 uart_close(int id, const struct uart_platform_data *data);

#endif


